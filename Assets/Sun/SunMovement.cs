using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunMovement : MonoBehaviour {
  [SerializeField] private float height;
  [SerializeField] private float trackRadius;
  [SerializeField] private float radius;
  [SerializeField] private float degreesPerSecond;

  private float angle = 0;
  private void Start() {
    transform.localScale = Vector3.one * radius;
  }

  private void Update() {
    angle += Time.deltaTime * degreesPerSecond;
    if (angle >= 360f) {
      angle -= 360f;
    }

    float radians = angle * Mathf.Deg2Rad;

    transform.localPosition = new Vector3(trackRadius * Mathf.Cos(radians), height, trackRadius * Mathf.Sin(radians));
  }
}
